package com.example.calculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.databinding.LinearLayoutBinding

class LinearActivity : AppCompatActivity() {
    private lateinit var binding: LinearLayoutBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LinearLayoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val displayEditText = binding.editText

        val digitButtons = listOf(
            binding.button1, binding.button2, binding.button3, binding.button4,
            binding.button5, binding.button6, binding.button7, binding.button8,
            binding.button9
        )

        digitButtons.forEach { button ->
            button.setOnClickListener {
                displayEditText.append(button.text)
            }
        }

        val operatorButtons = listOf(
            binding.buttonPlus, binding.buttonMinus, binding.buttonMultiply
        )

        operatorButtons.forEach { button ->
            button.setOnClickListener {
                displayEditText.append(button.text)
            }
        }
    }
}